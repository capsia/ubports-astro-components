class HeaderContainer extends HTMLElement {
  constructor() {
    super();

    this.onclick = (e) => {
      let headerNav = document.getElementById("headerNav");
      if (!e.target.closest(".header") && headerNav) {
        headerNav.classList.remove("show");
        this.classList.remove("backdrop");
      }
    };

    document.addEventListener("click", (e) => {
      if (
        !e.target.closest(".header-dropdown-list") &&
        !e.target.closest(".header-nav")
      ) {
        document
          .querySelectorAll(".header-dropdown-list")
          .forEach((d) => d.classList.add("d-none"));
      }
    });

    document.querySelectorAll(".header-dropdown-toggler").forEach((t) =>
      t.addEventListener("click", (e) => {
        document.querySelectorAll(".header-dropdown-list").forEach((d) => {
          if (!e.target.parentElement.contains(d)) d.classList.add("d-none");
        });
      })
    );
  }
}

export default HeaderContainer;
