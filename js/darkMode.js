export function setDarkMode(darkTheme, doc, persistPreference = true) {
  const el = doc.querySelector("body");
  if (darkTheme) {
    if (persistPreference) localStorage.setItem("darkMode", true);
    el.classList.add("theme-dark");
  } else {
    if (persistPreference) localStorage.setItem("darkMode", false);
    el.classList.remove("theme-dark");
  }
}

export function setDefaultColorMode(doc) {
  setDarkMode(isDarkMode(), doc, false);
}

export function isDarkMode() {
  return localStorage.getItem("darkMode") === null
    ? window.matchMedia &&
        window.matchMedia("(prefers-color-scheme: dark)").matches
    : !!(localStorage.getItem("darkMode") == "true");
}

class DarkMode extends HTMLElement {
  constructor() {
    super();

    setDefaultColorMode(document);

    this.onclick = () => {
      setDarkMode(!isDarkMode(), document);
    };

    window
      .matchMedia("(prefers-color-scheme: dark)")
      .addEventListener("change", (event) => {
        setDefaultColorMode(document);
      });
  }
}

export default DarkMode;
