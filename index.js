// Do not write code directly here, instead use the `src` folder!
// Then, use this file to export everything you want your user to access.

export { default as HeaderContainer } from './src/HeaderContainer.astro';
export { default as FooterContainer } from './src/FooterContainer.astro';
export { default as ThemedImage } from './src/ThemedImage.astro';
export { default as DarkMode } from './src/DarkMode.astro';
