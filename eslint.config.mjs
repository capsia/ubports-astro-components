import prettierConfig from "eslint-plugin-prettier/recommended";
import astroPlugin from "eslint-plugin-astro";

export default [
  {
    languageOptions: {
      ecmaVersion: "latest",
      sourceType: "module"
    }
  },
  ...astroPlugin.configs.recommended,
  prettierConfig,

  {
    files: ["**/*.astro"],
    rules: {
      "astro/no-unused-css-selector": "error",
      "astro/prefer-class-list-directive": "error",
      "astro/prefer-object-class-list": "error",
      "astro/prefer-split-class-list": "error"
    }
  }
];
